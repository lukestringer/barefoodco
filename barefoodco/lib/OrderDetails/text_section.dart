import 'package:flutter/material.dart';

class TextSection extends StatelessWidget {
  final String _foodName;
  final int _price;
  final String _foodDescription;
  final int _quantity;

  TextSection(this._foodName, this._price, this._foodDescription, this._quantity);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
      ),
      child:
        Text(this._foodName),
    );
  }
}