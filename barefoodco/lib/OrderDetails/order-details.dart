import 'package:flutter/material.dart';
import '../models/orders.dart';
import 'text_section.dart';

class OrderDetail extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // simply fetch the first location we have
    // NOTE: we'll be moving this to a scoped_model later
    final orders = Order.fetchAll();
    final order = orders.first;

    return Scaffold(
      appBar: AppBar(
        title: Text(order.orderDetails),
      ),
      body: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            // ImageBanner(order.imagePath),
          ]..addAll(textSections(order))),
    );
  }

  List<Widget> textSections(Order order) {
    return order.foods
        .map((food) => TextSection(food.foodName, food.price, food.foodDescription, food.quantity))
        .toList();
  }
}