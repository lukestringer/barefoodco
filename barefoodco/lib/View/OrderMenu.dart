import 'package:flutter/material.dart';
import 'package:barefoodco/models/orders.dart';
import 'package:flutter/src/widgets/basic.dart';

class OrderMenu extends StatefulWidget {
  @override
  OrderMenuState createState() => OrderMenuState();
}

class OrderMenuState extends State<OrderMenu> {
  int _count = 0;
  // FocusNode mySpecialRequest;
  
  String specialReq;
  OrderMenu _order;
  // @override
  // void initState() {
  //   super.initState();
  //   mySpecialRequest = FocusNode();
  // }

  // @override
  // void dispose() {
  //   mySpecialRequest.dispose();
  //   super.dispose();
  // }

  Widget _buildButton(IconData icon, String buttonTitle) {
      final Color tintColor = Colors.blue;
      // if (buttonTitle == "add") {
      //   new FloatingActionButton( onPressed: _incrementCount());
      // }
      // else {
      //   new FloatingActionButton( onPressed: _decrementCount()),
      // }
      return new Column(
        children: <Widget>[
          new Icon(icon, color: tintColor),
          new Container(
            child: new Text(buttonTitle),
          )
        ],
      );
    }

  Widget _buttonsSection() {
      return new Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          IconButton(
            icon: Icon(Icons.remove),
            tooltip: 'remove count by one',
            onPressed: () {
              setState(() {
                this._count-=1;
              });
            },
          ),
          Text('Count: $_count'),
          IconButton(
            icon: Icon(Icons.add),
            tooltip: 'add count',
            onPressed: () {
              setState(() {
                this._count += 1;
              });
            },
          ),
        ],
      );
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Add Item Order'),
      ),
      body: new Container (
        padding: new EdgeInsets.all(20.0),
        // child: new Form(
        //   key: this._formKey,
          child: new ListView(
            children: <Widget>[
              new TextFormField(
                keyboardType: TextInputType.text,
                decoration: new InputDecoration(
                  labelText: 'Special Request'
                ),
                onSaved: (String saved) {
                  this.specialReq = saved;
                },
              ),
              _buttonsSection(),
              new Container(
                child: new RaisedButton(
                  child: new Text(
                    'Submit',
                    style: new TextStyle(
                      color: Colors.white
                    ),
                  ),
                  // onPressed: this.submit,
                  color: Colors.blue,
                ),
              )
            ]
          ,)
          // ),
        )
    );
  }
}
