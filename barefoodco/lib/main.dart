import 'package:barefoodco/Widgets/NavBar.dart';
import 'package:flutter/material.dart';
// import 'OrderDetails/order-details.dart';
import 'View/OrderMenu.dart';
import 'View/Login.dart';
import 'Widgets/Maps.dart';
// import 'OrderDetails/text_section.dart';

void main() => runApp(MyApp());

const BareRoute = '/';
const OrderRoute = '/OrderMenu';
const NavBarRoute = '/NavBarWidget';
const TextSecRoute = '/text_section';
const MapRoute = '/MapSample';

/// This Widget is the main application widget.
class MyApp extends StatelessWidget {
  static const String _title = 'Flutter Code Sample';

  @override
  Widget build(BuildContext context) {
    // MapView.setApiKey(api_key);
    return MaterialApp(
      onGenerateRoute: _routes(),
    );
  }

  RouteFactory _routes() {
    return (setting) {
      // final Map<String, dynamic> arguments = setting.arguments;
      Widget screen;
      switch (setting.name) {
        case BareRoute:
          screen = Login();
          break;
        case OrderRoute:
          screen = OrderMenu();
          break;
        case NavBarRoute:
          screen = NavBarWidget();
          break;
        // case MapRoute:
        //   screen = MapPage();
        //   break;
        // case TextSecRoute:
        //   screen = TextSection();
        default:
          return null;
      }
      return MaterialPageRoute(builder: (BuildContext context) => screen);
    };
  }
}

class MyStatefulWidget extends StatefulWidget {
  MyStatefulWidget({Key key}) : super(key: key);

  @override
  _MyStatefulWidgetState createState() => _MyStatefulWidgetState();
}

class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  int _selectedIndex = 0;
  static const TextStyle optionStyle =
      TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
  static const List<Widget> _widgetOptions = <Widget>[
    Text(
      'Index 0: Home',
      style: optionStyle,
    ),
    Text(
      'Index 1: Business',
      style: optionStyle,
    ),
    Text(
      'Index 2: School',
      style: optionStyle,
    ),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('BottomNavigationBar Sample'),
      ),
      body: Center(
        child: _widgetOptions.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            title: Text(''),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.account_circle),
            title: Text(''),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.school),
            title: Text(''),
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.amber[800],
        onTap: _onItemTapped,
      ),
    );
  }
}
// import 'package:barefoodco/View/Login.dart';

// void main() => Login();
