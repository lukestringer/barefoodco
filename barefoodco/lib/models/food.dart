class Food {
  final int foodID;
  final String foodName;
  final int price;
  final String foodDescription;
  final int quantity;

  Food(this.foodID, this.foodName, this.price, this.foodDescription, this.quantity);
}

