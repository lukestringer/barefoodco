//models/orders.dart
import './food.dart';

class Order {
  final int id;
  final String orderDetails;
  final int totalPayment;
  final List<Food> foods;
  final String specialRequestion;
  final int quantity;

  Order(this.id, this.orderDetails, this.totalPayment, this.foods, this.specialRequestion, this.quantity);
  static List<Order> fetchAll() {
    return [
      Order(1, 'Salmon Rolls and bento', 20, 
        [Food(1, 'Salmon Rolls', 5, 'Contains salmon, cucumber and crabmeats', 3), 
          Food(2, 'Bento', 5, 'rice and eggs', 1)],'With extra chilly please', 2),
    ];
  }

  
}