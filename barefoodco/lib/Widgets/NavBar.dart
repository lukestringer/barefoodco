import 'package:barefoodco/View/OrderMenu.dart';
import 'package:barefoodco/main.dart';
import 'package:flutter/material.dart';

class NavBarWidget extends StatefulWidget {
  NavBarWidget({Key key}) : super(key: key);

  @override
  _NavBarState createState() => _NavBarState();
}

class _NavBarState extends State<NavBarWidget> {
  int _selectedIndex = 0;
  static const TextStyle optionStyle =
      TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
  static const List<Widget> _widgetOptions = <Widget>[
    Text(
      'Index 0: Home',
      style: optionStyle,
    ),
    Text(
      'Index 1: Order',
      style: optionStyle,
    ),
    Text(
      'Index 2: Profile',
      style: optionStyle,
    ),
    Text(
      'Index 3: School',
      style: optionStyle,
    ),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
      switch(_selectedIndex) {
        case 0:
        case 1:
          _onOrderTap(context);
          break;
        case 2:
          _onMapTap(context);
          break;
        case 3:
        default:
          return null;
    
      }
      // if (_selectedIndex == 0) {
      //  // Navigator.push(
      //    // context,
      //    // MaterialPageRoute(builder: (context) => ()),
      //   //);
      // } else if (_selectedIndex == 1) {

      // } else if (_selectedIndex == 2) {

      // } else if (_selectedIndex == 3) {

      // }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(

      body: Center(
        child: _widgetOptions.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: BottomNavigationBar(
        showSelectedLabels: false,
        showUnselectedLabels: false,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            title: Text(''),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.fastfood),
            title: Text(''),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.account_circle),
            title: Text(''),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.settings),
            title: Text(''),
          ),
        ],
        currentIndex: _selectedIndex,
        unselectedItemColor: Colors.amber[200],
        selectedItemColor: Colors.amber[800],
        onTap: _onItemTapped,
      ),
    );
  }

  _onOrderTap(BuildContext context) {
    Navigator.pushNamed(context, OrderRoute);
  }
  _onMapTap(BuildContext context) {
    Navigator.pushNamed(context, MapRoute);
  }
}